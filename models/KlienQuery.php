<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Klien]].
 *
 * @see Klien
 */
class KlienQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Klien[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Klien|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
