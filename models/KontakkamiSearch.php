<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kontakkami;

/**
 * KontakkamiSearch represents the model behind the search form of `app\models\Kontakkami`.
 */
class KontakkamiSearch extends Kontakkami
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Alamat', 'Email'], 'safe'],
            [['Kontak_kami'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kontakkami::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Kontak_kami' => $this->Kontak_kami,
        ]);

        $query->andFilterWhere(['like', 'Alamat', $this->Alamat])
            ->andFilterWhere(['like', 'Email', $this->Email]);

        return $dataProvider;
    }
}
