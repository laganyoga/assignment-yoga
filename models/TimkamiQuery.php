<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Timkami]].
 *
 * @see Timkami
 */
class TimkamiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Timkami[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Timkami|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
