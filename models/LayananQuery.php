<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Layanan]].
 *
 * @see Layanan
 */
class LayananQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Layanan[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Layanan|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
