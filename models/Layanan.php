<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "layanan".
 *
 * @property string $Layanan
 * @property string $Deskripsi
 */
class Layanan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'layanan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Layanan', 'Deskripsi'], 'required'],
            [['Deskripsi'], 'string'],
            [['Layanan'], 'string', 'max' => 11],
            [['Layanan'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Layanan' => 'Layanan',
            'Deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * {@inheritdoc}
     * @return LayananQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LayananQuery(get_called_class());
    }
}
