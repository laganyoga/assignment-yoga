<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artikel".
 *
 * @property string $Judul
 * @property string $Deskripsi
 * @property string $Sumber_Artikel
 *
 * @property Dashboard $dashboard
 * @property Klien $judul
 */
class Artikel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'artikel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Judul', 'Deskripsi', 'Sumber_Artikel'], 'required'],
            [['Deskripsi', 'Sumber_Artikel'], 'string'],
            [['Judul'], 'string', 'max' => 100],
            [['Judul'], 'unique'],
            [['Judul'], 'exist', 'skipOnError' => true, 'targetClass' => Klien::class, 'targetAttribute' => ['Judul' => 'Title']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Judul' => 'Judul',
            'Deskripsi' => 'Deskripsi',
            'Sumber_Artikel' => 'Sumber Artikel',
        ];
    }

    /**
     * Gets query for [[Dashboard]].
     *
     * @return \yii\db\ActiveQuery|DashboardQuery
     */
    public function getDashboard()
    {
        return $this->hasOne(Dashboard::class, ['Beranda' => 'Judul']);
    }

    /**
     * Gets query for [[Judul]].
     *
     * @return \yii\db\ActiveQuery|KlienQuery
     */
    public function getJudul()
    {
        return $this->hasOne(Klien::class, ['Title' => 'Judul']);
    }

    /**
     * {@inheritdoc}
     * @return ArtikelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArtikelQuery(get_called_class());
    }
}
