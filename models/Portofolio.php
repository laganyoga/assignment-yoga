<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "portofolio".
 *
 * @property string $Judul
 * @property string $Deskripsi
 * @property string $Layanan
 * @property string $Nama_Klien
 * @property string $Tanggal
 *
 * @property Tentangkami $judul
 * @property Pemesanan $pemesanan
 */
class Portofolio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portofolio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Judul', 'Deskripsi', 'Layanan', 'Nama_Klien', 'Tanggal'], 'required'],
            [['Deskripsi', 'Layanan', 'Nama_Klien'], 'string'],
            [['Tanggal'], 'safe'],
            [['Judul'], 'string', 'max' => 11],
            [['Judul'], 'unique'],
            [['Judul'], 'exist', 'skipOnError' => true, 'targetClass' => Tentangkami::class, 'targetAttribute' => ['Judul' => 'Judul']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Judul' => 'Judul',
            'Deskripsi' => 'Deskripsi',
            'Layanan' => 'Layanan',
            'Nama_Klien' => 'Nama Klien',
            'Tanggal' => 'Tanggal',
        ];
    }

    /**
     * Gets query for [[Judul]].
     *
     * @return \yii\db\ActiveQuery|TentangkamiQuery
     */
    public function getJudul()
    {
        return $this->hasOne(Tentangkami::class, ['Judul' => 'Judul']);
    }

    /**
     * Gets query for [[Pemesanan]].
     *
     * @return \yii\db\ActiveQuery|PemesananQuery
     */
    public function getPemesanan()
    {
        return $this->hasOne(Pemesanan::class, ['Nama' => 'Judul']);
    }

    /**
     * {@inheritdoc}
     * @return PortofolioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PortofolioQuery(get_called_class());
    }
}
