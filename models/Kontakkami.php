<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kontakkami".
 *
 * @property string $Alamat
 * @property int $Kontak_kami
 * @property string $Email
 *
 * @property Layanan $alamat
 * @property Klien $klien
 */
class Kontakkami extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kontakkami';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Alamat', 'Kontak_kami', 'Email'], 'required'],
            [['Kontak_kami'], 'integer'],
            [['Alamat'], 'string', 'max' => 11],
            [['Email'], 'string', 'max' => 100],
            [['Alamat'], 'unique'],
            [['Alamat'], 'exist', 'skipOnError' => true, 'targetClass' => Layanan::class, 'targetAttribute' => ['Alamat' => 'Layanan']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Alamat' => 'Alamat',
            'Kontak_kami' => 'Kontak Kami',
            'Email' => 'Email',
        ];
    }

    /**
     * Gets query for [[Alamat]].
     *
     * @return \yii\db\ActiveQuery|LayananQuery
     */
    public function getAlamat()
    {
        return $this->hasOne(Layanan::class, ['Layanan' => 'Alamat']);
    }

    /**
     * Gets query for [[Klien]].
     *
     * @return \yii\db\ActiveQuery|KlienQuery
     */
    public function getKlien()
    {
        return $this->hasOne(Klien::class, ['Title' => 'Alamat']);
    }

    /**
     * {@inheritdoc}
     * @return KontakkamiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new KontakkamiQuery(get_called_class());
    }
}
