<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Kontakkami]].
 *
 * @see Kontakkami
 */
class KontakkamiQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Kontakkami[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Kontakkami|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
