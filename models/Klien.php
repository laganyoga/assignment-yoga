<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "klien".
 *
 * @property string $Title
 * @property string $Deskripsi
 *
 * @property Artikel $artikel
 * @property Kontakami $title
 */
class Klien extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'klien';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Title', 'Deskripsi'], 'required'],
            [['Deskripsi'], 'string'],
            [['Title'], 'string', 'max' => 11],
            [['Title'], 'unique'],
            [['Title'], 'exist', 'skipOnError' => true, 'targetClass' => Kontakami::class, 'targetAttribute' => ['Title' => 'Alamat']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Title' => 'Title',
            'Deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * Gets query for [[Artikel]].
     *
     * @return \yii\db\ActiveQuery|ArtikelQuery
     */
    public function getArtikel()
    {
        return $this->hasOne(Artikel::class, ['Judul' => 'Title']);
    }

    /**
     * Gets query for [[Title]].
     *
     * @return \yii\db\ActiveQuery|KontakamiQuery
     */
    public function getTitle()
    {
        return $this->hasOne(Kontakami::class, ['Alamat' => 'Title']);
    }

    /**
     * {@inheritdoc}
     * @return KlienQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new KlienQuery(get_called_class());
    }
}
