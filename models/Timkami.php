<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "timkami".
 *
 * @property string $Judul
 * @property string $Deskripsi
 *
 * @property Tentangkami $tentangkami
 */
class Timkami extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timkami';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Judul', 'Deskripsi'], 'required'],
            [['Deskripsi'], 'string'],
            [['Judul'], 'string', 'max' => 11],
            [['Judul'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Judul' => 'Judul',
            'Deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * Gets query for [[Tentangkami]].
     *
     * @return \yii\db\ActiveQuery|TentangkamiQuery
     */
    public function getTentangkami()
    {
        return $this->hasOne(Tentangkami::class, ['Judul' => 'Judul']);
    }

    /**
     * {@inheritdoc}
     * @return TimkamiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TimkamiQuery(get_called_class());
    }
}
