<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tentangkami".
 *
 * @property string $Judul
 * @property string $Deskripsi
 *
 * @property Timkami $judul
 * @property Portofolio $portofolio
 */
class Tentangkami extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tentangkami';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Judul', 'Deskripsi'], 'required'],
            [['Deskripsi'], 'string'],
            [['Judul'], 'string', 'max' => 11],
            [['Judul'], 'unique'],
            [['Judul'], 'exist', 'skipOnError' => true, 'targetClass' => Timkami::class, 'targetAttribute' => ['Judul' => 'Judul']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Judul' => 'Judul',
            'Deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * Gets query for [[Judul]].
     *
     * @return \yii\db\ActiveQuery|TimkamiQuery
     */
    public function getJudul()
    {
        return $this->hasOne(Timkami::class, ['Judul' => 'Judul']);
    }

    /**
     * Gets query for [[Portofolio]].
     *
     * @return \yii\db\ActiveQuery|PortofolioQuery
     */
    public function getPortofolio()
    {
        return $this->hasOne(Portofolio::class, ['Judul' => 'Judul']);
    }

    /**
     * {@inheritdoc}
     * @return TentangkamiQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TentangkamiQuery(get_called_class());
    }
}
