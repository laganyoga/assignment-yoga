<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Portofolio;

/**
 * PortofolioSearch represents the model behind the search form of `app\models\Portofolio`.
 */
class PortofolioSearch extends Portofolio
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Judul', 'Deskripsi', 'Layanan', 'Nama_Klien', 'Tanggal'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Portofolio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Tanggal' => $this->Tanggal,
        ]);

        $query->andFilterWhere(['like', 'Judul', $this->Judul])
            ->andFilterWhere(['like', 'Deskripsi', $this->Deskripsi])
            ->andFilterWhere(['like', 'Layanan', $this->Layanan])
            ->andFilterWhere(['like', 'Nama_Klien', $this->Nama_Klien]);

        return $dataProvider;
    }
}
