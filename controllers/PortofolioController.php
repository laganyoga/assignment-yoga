<?php

namespace app\controllers;

use app\models\Portofolio;
use app\models\PortofolioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PortofolioController implements the CRUD actions for Portofolio model.
 */
class PortofolioController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Portofolio models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PortofolioSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Portofolio model.
     * @param string $Judul Judul
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($Judul)
    {
        return $this->render('view', [
            'model' => $this->findModel($Judul),
        ]);
    }

    /**
     * Creates a new Portofolio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Portofolio();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'Judul' => $model->Judul]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Portofolio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Judul Judul
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($Judul)
    {
        $model = $this->findModel($Judul);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Judul' => $model->Judul]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Portofolio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Judul Judul
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($Judul)
    {
        $this->findModel($Judul)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Portofolio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Judul Judul
     * @return Portofolio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Judul)
    {
        if (($model = Portofolio::findOne(['Judul' => $Judul])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
