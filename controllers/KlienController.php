<?php

namespace app\controllers;

use app\models\Klien;
use app\models\KlienSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * KlienController implements the CRUD actions for Klien model.
 */
class KlienController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Klien models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new KlienSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Klien model.
     * @param string $Title Title
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($Title)
    {
        return $this->render('view', [
            'model' => $this->findModel($Title),
        ]);
    }

    /**
     * Creates a new Klien model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Klien();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'Title' => $model->Title]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Klien model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $Title Title
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($Title)
    {
        $model = $this->findModel($Title);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'Title' => $model->Title]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Klien model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $Title Title
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($Title)
    {
        $this->findModel($Title)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Klien model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $Title Title
     * @return Klien the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($Title)
    {
        if (($model = Klien::findOne(['Title' => $Title])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
