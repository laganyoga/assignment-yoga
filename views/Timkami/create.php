<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Timkami $model */

$this->title = 'Create Timkami';
$this->params['breadcrumbs'][] = ['label' => 'Timkamis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timkami-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
