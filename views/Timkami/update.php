<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Timkami $model */

$this->title = 'Update Timkami: ' . $model->Judul;
$this->params['breadcrumbs'][] = ['label' => 'Timkamis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Judul, 'url' => ['view', 'Judul' => $model->Judul]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="timkami-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
