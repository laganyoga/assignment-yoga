<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Dashboard $model */
/** @var ActiveForm $form */
?>
<div class="dashboard">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'Beranda') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- dashboard -->
