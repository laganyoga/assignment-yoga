<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Kontakkami $model */

$this->title = $model->Alamat;
$this->params['breadcrumbs'][] = ['label' => 'Kontakkamis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="kontakkami-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Alamat' => $model->Alamat], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Alamat' => $model->Alamat], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Alamat',
            'Kontak_kami',
            'Email:email',
        ],
    ]) ?>

</div>
