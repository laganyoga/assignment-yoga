<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Kontakkami $model */

$this->title = 'Create Kontakkami';
$this->params['breadcrumbs'][] = ['label' => 'Kontakkamis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kontakkami-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
