<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Kontakkami $model */

$this->title = 'Update Kontakkami: ' . $model->Alamat;
$this->params['breadcrumbs'][] = ['label' => 'Kontakkamis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Alamat, 'url' => ['view', 'Alamat' => $model->Alamat]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kontakkami-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
