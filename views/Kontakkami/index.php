<?php

use app\models\Kontakkami;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\models\KontakkamiSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Kontakkami';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kontakkami-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Kontakkami', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Alamat',
            'Kontak_kami',
            'Email:email',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Kontakkami $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'Alamat' => $model->Alamat]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
