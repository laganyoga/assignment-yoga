<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Layanan $model */
/** @var ActiveForm $form */
?>
<div class="layanan">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'Layanan') ?>
        <?= $form->field($model, 'Deskripsi') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- layanan -->
