<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Layanan $model */

$this->title = 'Update Layanan: ' . $model->Layanan;
$this->params['breadcrumbs'][] = ['label' => 'Layanans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Layanan, 'url' => ['view', 'Layanan' => $model->Layanan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="layanan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
