<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Layanan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="layanan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Layanan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Deskripsi')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
