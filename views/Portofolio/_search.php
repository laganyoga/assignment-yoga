<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\PortofolioSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="portofolio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'Judul') ?>

    <?= $form->field($model, 'Deskripsi') ?>

    <?= $form->field($model, 'Layanan') ?>

    <?= $form->field($model, 'Nama_Klien') ?>

    <?= $form->field($model, 'Tanggal') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
