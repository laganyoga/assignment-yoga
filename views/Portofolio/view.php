<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Portofolio $model */

$this->title = $model->Judul;
$this->params['breadcrumbs'][] = ['label' => 'Portofolios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="portofolio-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'Judul' => $model->Judul], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'Judul' => $model->Judul], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Judul',
            'Deskripsi:ntext',
            'Layanan:ntext',
            'Nama_Klien:ntext',
            'Tanggal',
        ],
    ]) ?>

</div>
