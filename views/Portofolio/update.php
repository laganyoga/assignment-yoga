<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Portofolio $model */

$this->title = 'Update Portofolio: ' . $model->Judul;
$this->params['breadcrumbs'][] = ['label' => 'Portofolios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Judul, 'url' => ['view', 'Judul' => $model->Judul]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="portofolio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
