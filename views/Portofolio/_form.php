<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Portofolio $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="portofolio-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Judul')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Deskripsi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Layanan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Nama_Klien')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'Tanggal')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
