<?php

use app\models\Portofolio;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\models\PortofolioSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Portofolio';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portofolio-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Portofolio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Judul',
            'Deskripsi:ntext',
            'Layanan:ntext',
            'Nama_Klien:ntext',
            'Tanggal',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Portofolio $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'Judul' => $model->Judul]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
