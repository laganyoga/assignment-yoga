<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Tentangkami $model */

$this->title = 'Update Tentangkami: ' . $model->Judul;
$this->params['breadcrumbs'][] = ['label' => 'Tentangkamis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Judul, 'url' => ['view', 'Judul' => $model->Judul]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tentangkami-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
