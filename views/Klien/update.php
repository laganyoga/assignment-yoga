<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Klien $model */

$this->title = 'Update Klien: ' . $model->Title;
$this->params['breadcrumbs'][] = ['label' => 'Kliens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Title, 'url' => ['view', 'Title' => $model->Title]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="klien-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
