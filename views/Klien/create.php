<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Klien $model */

$this->title = 'Create Klien';
$this->params['breadcrumbs'][] = ['label' => 'Kliens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="klien-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
