<?php

use app\models\Artikel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\models\ArtikelSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Artikel';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="artikel-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Artikel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Judul',
            'Deskripsi:ntext',
            'Sumber_Artikel:ntext',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Artikel $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'Judul' => $model->Judul]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
