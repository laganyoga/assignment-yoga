<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Artikel $model */

$this->title = 'Update Artikel: ' . $model->Judul;
$this->params['breadcrumbs'][] = ['label' => 'Artikels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Judul, 'url' => ['view', 'Judul' => $model->Judul]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="artikel-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
